module.exports = [
  {
    summary: 'Our bathroom surfaces /3d',
    duration: 20
  },
  {
    summary: 'Our bathroom floor',
    duration: 10
  },
  {
    summary: 'Kids bathroom surfaces',
    duration: 20
  },
  {
    summary: 'Kids bathroom floor',
    duration: 10
  },
  {
    summary: 'Downstairs bathroom surfaces',
    duration: 20
  },
  {
    summary: 'Downstairs bathroom floor',
    duration: 10
  },
  {
    summary: 'Our surfaces',
    duration: 20
  },
  {
    summary: 'Our bed',
    duration: 20
  },
  {
    summary: 'Kids surfaces',
    duration: 10
  },
  {
    summary: 'Kids beds',
    duration: 20
  },
  {
    summary: 'Kids skirting /4w',
    duration: 20
  },
  {
    summary: 'Kitchen surfaces /1d',
    duration: 20
  },
  {
    summary: 'Kitchen floor',
    duration: 20
  },
  {
    summary: 'Lounge surfaces',
    duration: 20
  },
  {
    summary: 'Lounge skirting /2w',
    duration: 20
  },
  {
    summary: 'Lounge floor',
    duration: 20
  },
  {
    summary: 'Dining room floor',
    duration: 20
  },
  {
    summary: 'Dining room skirting /2w',
    duration: 20
  }
]
