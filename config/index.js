module.exports = {
  title: 'Chores 🧹',
  // Client ID and API key from the Developer Console
  clientId: '1018048405199-urfev6k0pbf5hvkecalg87nk4ab1l4qq.apps.googleusercontent.com',
  apiKey: 'AIzaSyAl21iyJ6I3_hpI0pSt-qD7ZtKFXbpCgUo',
  // maxResults: 20,
  itemIcon: `🧹`,
  orderBy: 'startTime',
  singleEvents: true,
  calendarId: 'primary',
  showDeleted: false,
  defaultDuration: 5, // allow 5 minutes for a simple task
  tooEarly: 6, // hour before which not to set an event
  tooLate: 22, // hour after which not to set an event
  // Array of API discovery doc URLs for APIs used by the quickstart
  discoveryDocs: ['https://www.googleapis.com/discovery/v1/apis/calendar/v3/rest'],
  // Authorization scopes required by the API; multiple scopes can be
  // included, separated by spaces.
  scope: 'https://www.googleapis.com/auth/calendar'
}
