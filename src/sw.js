/* global self caches fetch */
((self, caches) => {
  const cacheName = process.env.npm_package_name + '-' + process.env.npm_package_version

  const pathsToCache = [
    '/',
    '/css/body.css',
    '/manifest.json',
    '/img/icons-72.png',
    '/img/icons-144.png',
    '/img/icons-192.png',
    '/faq'
  ]

  function on (element, event, callback) {
    element.addEventListener(event, callback)
  }

  on(self, 'install', e => {
    e.waitUntil(
      caches.open(cacheName).then(cache => {
        console.log(`adding to ${cacheName}`)
        return cache.addAll(pathsToCache).then(() => self.skipWaiting())
      })
    )
  })

  on(self, 'activate', event => {
    event.waitUntil(self.clients.claim())

    event.waitUntil(
      caches.keys().then((keyList) => {
        return Promise.all(keyList.map((key) => {
          if (key !== cacheName) return caches.delete(key)
        }))
      })
    )
  })

  on(self, 'fetch', event => {
    var request = event.request

    event.respondWith(
      caches.match(request)
        .then(response => {
          if (response) console.log(response.url, `was in cache ${cacheName}`)
          else console.log(request.url, `was not in cache ${cacheName}`)
          return fetch(request)
            /* .then(liveResponse => {
              console.log('update cache with', liveResponse, '???')
              // console.log(liveResponse.headers.entries())
              if (/text/.test(liveResponse.headers.get('content-type'))) {
                caches.open(cacheName).then(cache => {
                  console.log('updated cache with', request, liveResponse)
                  cache.put(request, liveResponse)
                })
              }
              return liveResponse
            }) */
            .catch(error => { // always try and get the live one
              console.warn(error, 'could not get', request.url, 'try returning', response)
              return response
            })
        })
    )
  })
})(self, caches)
