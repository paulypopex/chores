const lib = module.exports = {}

const oneMinute = 60000
const oneDay = oneMinute * 60 * 24

// get the start of a calendar event
lib.eventStart = event => event && event.start && (event.start.dateTime || event.start.date)

// get the end of a calendar event
lib.eventEnd = event => event && event.end && (event.end.dateTime || event.end.date)

// cheap and clumsy formatting of a date or date-time
lib.formatDate = date => {
  const lastPart = /^\d\d\d\d-\d\d-\d\d$/.test(date) ? 2 : 3
  return ('' + new Date(date)).split(' ').slice(1, lastPart).join(' ')
}

lib.datePlusMinutes = (date, minutes) => {
  const originalDate = date ? new Date(date) : new Date()
  return new Date(originalDate.getTime() + minutes * oneMinute)
}

lib.estimatePerDay = (event, reminder) => {
  const eventDurationInDays = Math.floor((new Date(lib.eventEnd(event)) - new Date(lib.eventStart(event))) / oneDay)
  const match = /\bx(\d+)\b/.exec(reminder.summary)
  return match ? match[1] / eventDurationInDays : undefined // undefined so does not appear in export object
}

lib.updateQuantities = (reminder, eventDurationInDays) => {
  const regex = /\bx(\d+)\b/g
  const match = regex.exec(reminder.summary)
  if (!match) return reminder
  const quantity = Math.round(reminder.perDay * eventDurationInDays)
  reminder.summary = reminder.summary.replace(regex, `x${quantity}`)
  return reminder
}

lib.reminderListItem = reminder => {
  const now = (new Date()).toISOString()
  const eventStart = lib.eventStart(reminder)
  const past = eventStart < now
  const done = !(reminder.reminders && reminder.reminders.overrides && reminder.reminders.overrides.length) && !past
  const warning = past && !done ? '⚠️' : ''
  const checked = done ? 'checked' : ''
  const checkbox = `<input type="checkbox" id="${reminder.id}" ${checked}/>`
  // console.log({ now, eventStart, past, done, warning, checked, checkbox }, 'reminderListItem')
  return `<li><label>${checkbox} ${warning} ${reminder.summary}</label> (${lib.formatDate(lib.eventStart(reminder))}) <a href="${reminder.htmlLink}">edit</a></li>`
}
