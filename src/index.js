/* global gapi, location */

const { apiKey, calendarId, clientId, defaultDuration, discoveryDocs, itemIcon, orderBy, scope, singleEvents, showDeleted, title, tooEarly, tooLate } = require('../config')
const defaultEvents = require('../config/data')
const { datePlusMinutes, estimatePerDay, eventStart, eventEnd, reminderListItem } = require('./lib')
const { html, id, on } = require('./js')
const oneMinute = 60000 // in milliseconds
const redirectTimeout = 2000 // time to redirect
const publicCalendarId = 'fa473btu4jso8h8ugprkfpa87c@group.calendar.google.com'
const publicEventId = '2d9i7ofopd2iu0s0on8qhbif7e'

/**
 *  On load, called to load the auth2 library and API client library.
 */
module.exports = () => {
  on(window, 'load', () => {
    gapi.load('client:auth2', initClient)
  })
  on(document, 'click', event => {
    const { type, id, checked } = event.target || event.srcElement
    if (type === 'checkbox') delayedCheck(id, checked)
  })
}

/**
 *  Initialises the API client library and sets up sign-in state
 *  listeners.
 */
const initClient = () => {
  gapi.client.init({ apiKey, clientId, discoveryDocs, scope }).then(() => {
    // Listen for sign-in state changes.
    gapi.auth2.getAuthInstance().isSignedIn.listen(updateSigninStatus)

    // Handle the initial sign-in state.
    updateSigninStatus(gapi.auth2.getAuthInstance().isSignedIn.get())
    on(id('authorise'), 'click', authorise)

    // start watching for hash change
    window.onhashchange = change
  })
}

/**
 *  Called when the signed in status changes, to update the UI
 *  appropriately. After a sign-in, the API is called.
 */
const updateSigninStatus = isSignedIn => {
  id('authorise').style.display = isSignedIn ? 'none' : 'block'
  id('signout').style.display = isSignedIn ? 'list-item' : 'none'
  isSignedIn ? change() : updateContent()
}

/**
 *  Sign in the user upon button click.
 */
const authorise = event => {
  gapi.auth2.getAuthInstance().signIn()
}

/**
 *  Sign out the user upon button click.
 */
const signout = callback => {
  gapi.auth2.getAuthInstance().signOut()
  callback(null, 'Signed you out', '', '', redirectTimeout)
}

/**
 * Append a pre element to the body containing the given message
 * as its text node. Used to display the results of the API call.
 *
 * @param {string} message Text to be placed in pre element.
 */
const updateContent = (err, title = '', message = '', whereNext, when) => {
  let content = null
  if (err) {
    content = `<p>⚠️ ${err}</p>`
  } else {
    content = message
    if (title) content = `<h2>${title}</h2>${message}`
  }
  html(id('content'), content)
  if (whereNext || when) {
    setTimeout(() => {
      location.hash = whereNext
    }, when)
  }
}

const getEvent = (calendarId, eventId) => {
  if (eventId === publicEventId) calendarId = publicCalendarId
  return gapi.client.calendar.events.get({ calendarId, eventId })
    .then(response => response.result)
}

const getEvents = (calendarId, q) => {
  return gapi.client.calendar.events.list({ calendarId, q, showDeleted, singleEvents, orderBy })
    .then(response => {
      const events = response.result.items
      if (!events.length) return []
      return events
    }).catch(() => {
      return []
    })
}

const exportData = (eventId, callback) => {
  if (eventId === 'default') return callback(null, defaultEvents)
  getEvent(calendarId, eventId).then(event => {
    if (!event) return callback(null, [])
    const q = itemIcon
    const date = eventStart(event)
    getEvents(calendarId, q).then(reminders => {
      const data = reminders.map(reminder => {
        const offset = (new Date(eventStart(reminder)) - new Date(date)) / oneMinute
        let duration = (new Date(eventEnd(reminder)) - new Date(eventStart(reminder))) / oneMinute
        if (duration < defaultDuration) duration = undefined
        const summary = reminder.summary
        const description = reminder.description.replace(q, '').trim() || undefined
        const perDay = estimatePerDay(event, reminder)
        return { description, duration, offset, perDay, summary }
      })
      callback(null, data)
    })
  })
}

const exportReminders = (eventId, callback) => {
  exportData(eventId, (err, data) => {
    callback(err, 'Export', `<pre>${JSON.stringify(data, null, 2)}</pre>`)
  })
}

const deleteReminders = (eventId, confirm, callback) => {
  getEvents(calendarId, itemIcon).then(reminders => {
    if (reminders.length && !confirm) {
      const message = `Delete ${reminders.length} reminders? <a href="#delete/${eventId}/confirm">Click to confirm</a>`
      return callback(message)
    }
    reminders.forEach(reminder => {
      gapi.client.calendar.events.delete({ calendarId, eventId: reminder.id }).execute()
    })
    callback(null, `Deleted ${reminders.length} reminders`, `<p>${reminders.map(reminder => reminder.summary).join(', ')}</p>`, `event/${eventId}`, redirectTimeout)
  })
}

// keep retrying but with a bigger gap each time
const delayedInsert = (calendarId, resource, delay = 0) => {
  if (delay > 5000) return console.error('too many attempts, give up')
  delay += Math.floor(Math.random() * 300)
  setTimeout(() => {
    gapi.client.calendar.events.insert({ calendarId, resource }).execute(response => {
      switch (response.code) {
        case 403:
        case 503:
          console.warn(response)
          return delayedInsert(calendarId, resource, delay)
      }
    })
  }, delay)
}

const reminderPopUps = checked => {
  const overrides = checked ? [] : [
    // { method: 'email', minutes: 10 },
    { method: 'popup', minutes: 0 }
  ]
  const useDefault = false
  return { useDefault, overrides }
}

const insertReminder = (calendarId, reminder) => {
  const summary = reminder.summary
  const descriptionParts = [
    reminder.description,
    // `${Math.abs(Math.round(reminder.offset / 60))} hours before`,
    itemIcon
  ]
  const description = descriptionParts.join('\n').trim()
  const start = { dateTime: datePlusMinutes(eventStart(reminder), 0) }
  if (start.dateTime.getHours() < tooEarly) {
    start.dateTime.setDate(start.dateTime.getDate() - 1)
    start.dateTime.setHours(tooLate - 1)
  }
  if (start.dateTime.getHours() >= tooLate) {
    start.dateTime.setHours(tooLate - 1)
  }
  const duration = reminder.duration || defaultDuration // assume 5 mins for a simple task
  const end = { dateTime: datePlusMinutes(start.dateTime, duration) }
  const reminders = reminderPopUps()
  const url = location.origin
  const source = { url, title }
  const resource = { description, start, end, reminders, source, summary }
  delayedInsert(calendarId, resource)
}

const intervalInDays = reminder => {
  const match = /\/(\d+)?([dmwy])/.exec(reminder.summary)
  if (match) {
    const days = match[1] || 1
    // console.log(days, match, reminder.summary, reminder)
    if (match[2] === 'y') return days * 365
    if (match[2] === 'm') return days * 30
    if (match[2] === 'w') return days * 7
    return days
  }
  return 7
}

// keep retrying but with a bigger gap each time
// @todo merge with 👆
const delayedCheck = (eventId, checked, delay = 0) => {
  getEvent(calendarId, eventId).then(resource => {
    if (!resource) return
    resource.reminders = reminderPopUps(checked)
    resource.start = {
      dateTime: checked ? datePlusMinutes(eventStart(resource), intervalInDays(resource) * 24 * 60) : new Date()
    }
    resource.end = { dateTime: datePlusMinutes(eventStart(resource), 0) }
    console.log({ checked }, eventStart(resource), eventEnd(resource))
    delay += Math.floor(Math.random() * 100)
    if (delay > 5000) return console.error('too many attempts, give up')
    setTimeout(() => {
      gapi.client.calendar.events.patch({ calendarId, eventId, resource }).execute(response => {
        if (response.code === 503) delayedCheck(eventId, checked, delay)
      })
    }, delay)
  })
}

const addReminder = (eventId, offset, summary, callback) => {
  on(id('submit'), 'click', e => {
    e.preventDefault()
    addReminder(eventId, id('offset').value, id('summary').value, callback)
  })
  const reminder = { offset, summary, location }
  getEvent(calendarId, eventId).then(event => {
    if (!event) return callback()
    if (!offset || !summary) {
      id('reminder').style.display = 'block'
      return callback(null, 'Add reminder', `<p>Add reminder to <a href="#event/${eventId}">${event.summary}</a></p>`)
    }
    insertReminder(calendarId, reminder)
    callback(null, 'Added reminder', `<p>Added reminder to <a href="#event/${eventId}">${event.summary}</a></p>`, `event/${eventId}`, redirectTimeout)
  })
}

const importReminders = (eventId, eventIdToImportFrom, callback) => {
  copyEvents('default', eventId, callback)
}

const copyEvents = (eventIdToImportFrom, eventId, callback) => {
  exportData(eventIdToImportFrom, (err, reminders) => {
    console.log({ err, reminders })
    if (err) return callback(err)
    let timeout = 0
    reminders.forEach(reminder => {
      setTimeout(() => {
        insertReminder(calendarId, reminder)
      }, timeout)
      timeout += Math.floor(Math.random() * 300)
    })
    callback(null, `Added ${reminders.length} reminders`, `<p>Added ${reminders.map(reminder => reminder.summary).join(', ')}</p>`, `event/${eventId}`, redirectTimeout)
  })
}

const setup = callback => {
  copyEvents('6qovnisd9srlncfa6rapp1gtg8', publicEventId, callback)
}

const listReminders = (eventId, callback) => {
  getEvents(calendarId, itemIcon).then(reminders => {
    const list = reminders.map(reminderListItem).join('')
    const todo = reminders.length
    const done = reminders.filter(reminder => reminder.reminders.overrides).length
    const percent = todo ? Math.round(((todo - done) / todo) * 100) : (done ? 100 : 0)
    const content = `<ul>
      ${list}
      <li><a href="#add">🆕 Insert reminder</a> - add a new reminder</li>
      <li><a href="#import">⬅️ Import reminders</a> - drag in the preset reminders</li>
      <!-- <li><a href="#export">➡️ Export reminders</a> - output all the data, in case we can reuse it</li> -->
      ${todo ? '<li><a href="#delete">🚮 Delete reminders</a> - in case you made a mess, delete all the reminders</li>' : ''}
    </ul>`
    callback(null, `${percent}% done`, content)
  })
}

const change = () => {
  window.scroll(0, 0)
  if (!gapi.auth2.getAuthInstance().isSignedIn.get()) return // nothing for you here yet
  const hash = location.hash
  const path = hash.substr(1).split('/')
  id('reminder').style.display = 'none'
  switch (path[0]) {
    case 'add': return addReminder(path[1], null, null, updateContent)
    case 'import': return importReminders(null, null, updateContent)
    case 'export': return exportReminders(path[1], updateContent)
    case 'delete': return deleteReminders(path[1], path[2], updateContent)
    case 'setup': return setup(updateContent)
    case 'signout': return signout(updateContent)
  }
  listReminders(path[1], updateContent)
}

const _trackError = event => {
  const message = (event && event.message) || event
  const info = (event && event.error && event.error.stack) || event
  console.error({ message, code: 'uncaught', info })
}

on(window, 'error', _trackError)

if ('serviceWorker' in navigator) {
  const name = process.env.CI_PROJECT_NAME
  const path = name ? `/${name}` : ''
  const serviceWorker = name ? `sw.min.js` : `sw.js`
  if (name) navigator.serviceWorker.register(`${path}/${serviceWorker}`, { scope: `${path}/` })
  /*  .then(function(registration) {
      console.log('Service Worker Registered');
    })
  navigator.serviceWorker.ready.then(function(registration) {
    console.log('Service Worker Ready')
  }) */
}
